# Construção de uma API e boas praticas

# API - Comunica o frontend com o backend
# REST - Algumas regras
# RESTFUL - Aplicaçao das regras REST
# Insomnia/Postman -> Para se comunicar com a aplicação do backend atraves do protocolo http

# https://www.youtube.com/watch?v=ghTrp1x_1As

yarn init -y
yarb add express

node server.js


# Verbos HTTP mais usados
// GET -> Recebe dados de um resource.
// POST -> Envia dados ou informações para serem processados por um resource.
// PUT -> Atualiza os dados de um resource
// DELETE -> Deleta os dados de um resource

# URL/ENDPOINT -> http://localhost:3000/clients

# https://jsonplaceholder.typicode.com/users - Retorna alguns usuários para testes

# Status das respostas
- 1xx: informação
- 2xx: Sucesso
    - 200: OK
    - 201: CREATED
    - 204: Não tem conteúdo PUT, POST e DELETE
- 3xx: Redirection
- 4xx: Client Error
    - 400: Bad Request
    - 404: Not Found
- 5xx: Server Error
    - 500: Internal server error